from django.core.urlresolvers import reverse, resolve
from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User
from www.todolist.forms import TodoEntry_form
from www.todolist.models import TodoEntry 
import datetime
from django.utils import timezone
import logging


def debug(obj):
    logging.basicConfig()
    testlog = logging.getLogger("TESTLOG")
    log.warning(obj)

class MigrationTests(TestCase):
    def test_mitgrations(self):
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        self.assertEqual(User.objects.all().count(), 2)    
    
class Redirect_Test(TestCase):
    def test_overview(self):
        response = self.client.get(reverse("overview"))
        self.assertEqual(response.status_code, 302)

class LoginTest(TestCase):
    def test_invalidlogin(self):
        self.client = Client()
        self.client.login(username="wronguser", password="wrongpassword")
        response = self.client.get(reverse("overview"))
        self.assertEqual(response.status_code, 302)
        #302 > Redirect > try login again

    def test_validlogin(self):
        self.client = Client()
        self.client.login(username="test", password="test")
        response = self.client.get(reverse("overview"))
        self.assertEqual(response.status_code, 200)
        # 302 > Redirect > to Overview

class Logout(TestCase):
    def test_valid_login1(self):
        self.client = Client()
        self.client.login(username="test", password="test")
        # click Logout
        response = self.client.get('/todolist/logout')
        self.assertEqual(response.status_code, 301)
        # 302 > Redirect > to the site login
        response = self.client.get('/todolist/')
        self.assertEqual(response.status_code, 200)


class AddTest(TestCase):
    # setup method is called before each test
    def setUp(self):
        self.client = Client()

    def test_valid_add(self):
        self.client = Client()
        self.client.login(username="test",
                          password="test")
        self.client.get(reverse("add"))
        # Add new entry with valid data
        data = {
            'title': 'Test',
            'description': 'Test',
            'due_date': '2016-01-01',
            'due_time': '1:00'
        }
        response = self.client.post(reverse("add"), data=data)
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        # check the Nr of Entries after valid'add' (3+1)

    def test_invalidadd(self):
        self.client = Client()
        self.client.login(username="test",
                          password="test")
        self.client.get(reverse("add"))
        # Add new entry without Date und Time
        data = {
            'title': 'Test2',
            'description': 'Test'
        }
        response = self.client.post(reverse("add"))
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        # check the Nr of Entries after 'add' (3+0)

    def test_invalidadd(self):
        self.client = Client()
        self.client.login(username="test",
                          password="test")
        self.client.get(reverse("add"))
        # Add new entry without Date und Time
        data = {
            'title': 'Test3',
            'description': 'Test',
            'due_date': '18.04.2015',
            'due_time': '23:45'
        }
        response = self.client.post(reverse("add"))
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        # check the Nr of Entries after 'add' (3+0)

    def test_csrf(self):
        self.client = Client(enforce_csrf_checks=True)
        self.client.login(username="test",
                          password="test")
        data = {
            'title': 'CSRF',
            'description': 'CSRF',
            'due_date': '2015-04-17',
            'due_time': '12:00',
        }
        response = self.client.post(reverse("add"), data=data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(TodoEntry.objects.all().count(), 3)

class DeleteTest(TestCase):
    def deletetest_v1(self):
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        self.client = Client()
        self.client.login(username="test",
                          password="test")
        response = self.client.get('/todolist/')
        self.assertTrue('done_list' in response.context)
        count_start = len(response.context['done_list'])
        entry = response.context['done_list']

        self.client.get(reverse('delete', kwargs={'entry_id': 1}))

        response = self.client.get('/todolist/')
        self.assertTrue('done_list' in response.context)
        count_end = len(response.context['done_list'])
        self.assertFalse(count_start > count_end)
        self.assertEqual(TodoEntry.objects.all().count(), 2)

    def deletetest_v2(self):
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        self.client = Client()
        self.client.login(username="test",
                          password="test")
        response = self.client.get('/todolist/')
        self.client.get(reverse('delete', kwargs={'entry_id': 1}))
        self.assertEqual(TodoEntry.objects.all().count(), 2)
        #Testuser can delete only his entries

class deleteFromOtherUser(TestCase):
    def deleteFromOtherUser(self):
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        self.client = Client()
        self.client.login(username="test2",
                          password="test2")
        response = self.client.get('/todolist/')
        self.client.get(reverse('delete', kwargs={'entry_id': 1}))
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        #Delete from Other User is not posibble
        #Every User sees only his own entries

class SwitchTest(TestCase):
    # setup method is called before each test
    def test_switch(self):
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        self.client = Client()
        self.client.login(username="test",
                          password="test")
        response = self.client.get('/todolist/overview')
        self.assertTrue('open_entries' in response.context)
        self.assertFalse(len(response.context['open_entries']) == 2)
        self.client.get(reverse('switch', kwargs={'id': 2}))
        self.assertFalse(len(response.context['open_entries']) == 2)

class ChangeDB(TestCase):
    def test_dbChange(self):
        tentry = TodoEntry.objects.get(pk=1)
        self.assertEquals(tentry.entry_id, 1)
        tentry.title = 'TESTING!!!'
        tentry.due_date = '2015-07-16'
        tentry.due_time = '22:00'
        tentry.save()
        self.assertEquals(tentry.title, 'TESTING!!!')

class Urlresolvtest(TestCase):
    def test_urlresolv(self):
        resolver = resolve('/todolist/overview')
        self.assertEqual(resolver.view_name, 'overview')

class FixtureTests(TestCase):
    def test_entries(self):
        # check for migrations
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        # check if fixture created 2 users
        self.assertEqual(User.objects.all().count(), 2)