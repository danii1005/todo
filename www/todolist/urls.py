from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    url(r'^overview$', views.overview, name='overview'),
    url(r'^add', views.add, name='add'),
    url(r'^edit/(?P<entry_id>[0-9]+)/$', views.edit, name='edit'),
    url(r'^delete/(?P<entry_id>[0-9]+)/$', views.delete, name='delete'),
    url(r'^switch/(?P<entry_id>[0-9]+)/$', views.switch, name='switch'),
    # urls for login and logout
    url(r'^$', auth_views.login, {"template_name": "login.html"}, name='login'),
    url(r'^logout/$', auth_views.logout, {"next_page": "overview"}, name='logout'),
]
