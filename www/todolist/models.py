from django.db import models

from django.contrib.auth.models import User
from datetime import date, datetime

# Create your models here.

# I use the users class, which is predifened and contains the user_id (as primary key),
# a username, a password, the users firstname, lastname, email address,
# booleanfields for: is_staff, is_active and is_superuser and a last_login field.
# The fields, firstname, lastname and e-mail are optional and are allowed to be blank.

# predefined Users: username, password, firstname, lastname, email, is_staff, is_active, is_superuser, last_login

# The TodoEntry class contains the entry_id as primary key,
# the user_id as a foreign_key from the predefined Users class,
# the title of the entry, an optional description,
# a priority, with one of 4 possible choices (0=default, NONE),
# a due_date and time (requires input default=datetime.todayin form of: YYYY-MM-DD and HH:MM)
# as well as the creation_datetime timestamp.
# The status field has 2 possible Choices, DONE or OPEN.


class TodoEntry(models.Model):

    entry_id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(User)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=500, blank=True)
    PRIORITY_CHOICES = (
        ('KEINE', 'KEINE'),
        ('NIEDRIG', 'NIEDRIG'),
        ('MITTEL', 'MITTEL'),
        ('HOCH', 'HOCH'),
    )
    priority = models.CharField(max_length=7, choices=PRIORITY_CHOICES, default='N')
    due_date = models.DateField('Due date')
    due_time = models.TimeField('Due time')
    creation_datetime = models.DateTimeField('Creation date and time', auto_now_add=True)
    STATUS_CHOICES = (
        ('O', 'OPEN'),
        ('D', 'DONE'),
    )
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default=0)

    # To display a useful text (the title of the entry) in the
    # interactive prompt instead of <TodoEntry: TodoEntry object>
    def __unicode__(self):
        return self.title
