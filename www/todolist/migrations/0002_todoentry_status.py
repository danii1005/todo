# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='todoentry',
            name='status',
            field=models.CharField(default=0, max_length=1, choices=[(b'OPEN', b'O'), (b'DONE', b'D')]),
        ),
    ]
