# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0003_auto_20150412_1239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todoentry',
            name='priority',
            field=models.CharField(default=b'N', max_length=7, choices=[(b'KEINE', b'KEINE'), (b'NIEDRIG', b'NIEDRIG'), (b'MITTEL', b'MITTEL'), (b'HOCH', b'HOCH')]),
        ),
        migrations.AlterField(
            model_name='todoentry',
            name='status',
            field=models.CharField(default=0, max_length=1, choices=[(b'O', b'OPEN'), (b'D', b'DONE')]),
        ),
    ]
