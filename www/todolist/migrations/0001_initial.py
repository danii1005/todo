# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='TodoEntry',
            fields=[
                ('entry_id', models.AutoField(serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=500, blank=True)),
                ('priority', models.CharField(default=b'N', max_length=1, choices=[(b'NONE', b'N'), (b'LOW', b'L'), (b'MEDIUM', b'M'), (b'HIGH', b'H')])),
                ('due_date', models.DateField(verbose_name=b'Due date')),
                ('due_time', models.TimeField(verbose_name=b'Due time')),
                ('creation_datetime', models.DateTimeField(auto_now_add=True, verbose_name=b'Creation date and time')),
                ('user_id', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
