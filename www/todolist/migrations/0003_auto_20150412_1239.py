# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.contrib.auth.models import User
from datetime import date, time


# Creates the function to fill the Database with the testvalues
def testvalues(apps, schema_editor):
    # import the TodoEntry model
    TodoEntry = apps.get_model("todolist","TodoEntry")
    # import the User model, to get the predefined User entry
    userobj = apps.get_model("auth", "user")
    # get the User_object with the id=1 (Superuser)
    User.objects.create_user('test', 'test@mail.com', 'test')
    User.objects.create_user('test2', 'test2@mail.com', 'test2')
    user = userobj.objects.get(id=1)

    # Add the Entries to the Database, user=Superuser, description can be blank
    TodoEntry(user_id=user,
        title="Erster Eintrag",
        description="",
        priority="LOW",
        due_date='2015-04-25',
        due_time='08:00',
        status="O",
        ).save()

    TodoEntry(user_id=user,
        title="Zweiter Eintrag",
        description="blubb",
        priority="MEDIUM",
        due_date='2015-04-26',
        due_time='08:00',
        status="D",
        ).save()

    TodoEntry(user_id=user,
        title="Dritter Eintrag",
        description="test",
        priority="HIGH",
        due_date='2015-04-27',
        due_time='8:00',
        status="O",
        ).save()


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0002_todoentry_status'),
    ]

    operations = [
        # Start the Datamigration of function testvalues
        migrations.RunPython(testvalues)
    ]
