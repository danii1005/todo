from django.shortcuts import render, get_object_or_404, redirect, Http404
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.template.loader import get_template
from django.contrib.auth.models import User

from www.todolist.forms import TodoEntry_form
from www.todolist.models import TodoEntry


# Create your views here.
# I mixed alternative ways of rendering the views into the forms

# login required, if not logged in -> redirect to the login view
@login_required(login_url='django.contrib.auth.views.login')
def overview(request):
    # get all open/done entries, where the user_id equals the user who is logged in
    entries = get_template('overview.html').render({
        'done_list': TodoEntry.objects.filter(
            user_id=request.user,
            status="D"
            ),
        'open_list': TodoEntry.objects.filter(
            user_id=request.user,
            status="O"
            ),
        })
    return HttpResponse(entries)


@login_required(login_url='django.contrib.auth.views.login')
def add(request):
    # add form, if post method->create new entry, else show empty form
    if request.method == 'POST':
        form = TodoEntry_form(data=request.POST)
        if form.is_valid():
            user_id = request.user
            title = request.POST.get('title')
            description = request.POST.get('description')
            priority = request.POST.get('priority')
            due_date = request.POST.get('due_date')
            due_time = request.POST.get('due_time')

            new_entry = TodoEntry(
                user_id=user_id,
                title=title,
                description=description,
                priority=priority,
                due_date=due_date,
                due_time=due_time,
                status="O",
                )
            new_entry.save()

            return redirect('overview')
    else:
        form = TodoEntry_form()

    return render(request, 'form.html', {'form': form})


@login_required(login_url='django.contrib.auth.views.login')
def edit(request, entry_id):
    # get the entry and fill the formular with it, same as above
    edit_entry = get_object_or_404(TodoEntry, entry_id=entry_id, user_id=request.user)
    if request.method == 'POST':
        form = TodoEntry_form(data=request.POST)
        if form.is_valid():
            user_id = request.user
            title = request.POST.get('title')
            description = request.POST.get('description')
            priority = request.POST.get('priority')
            due_date = request.POST.get('due_date')
            due_time = request.POST.get('due_time')

            TodoEntry.objects.filter(entry_id=entry_id).update(
                user_id=user_id,
                title=title,
                description=description,
                priority=priority,
                due_date=due_date,
                due_time=due_time,
                )
            return redirect('overview')
    else:
        form = TodoEntry_form(instance=edit_entry)

    return render(request, 'form.html', {'edit_entry': edit_entry, 'form': form})


@login_required(login_url='django.contrib.auth.views.login')
def delete(request, entry_id):
    # old and alternative way to post an 404 error.
    try:
        delete_entry = TodoEntry.objects.get(entry_id=entry_id)
    except TodoEntry.DoesNotExist:
            raise Http404("Cannot delete Entry that does not exist", user_id=request.user)
    delete_entry.delete()
    return redirect('overview')


@login_required(login_url='django.contrib.auth.views.login')
def switch(request, entry_id):
    # switch between open and done, alternative 404
    try:
        switch_entry = TodoEntry.objects.get(entry_id=entry_id, user_id=request.user)
    except TodoEntry.DoesNotExist:
            raise Http404("Cannot switch Entry that does not exist")
    if(switch_entry.status == 'O'):
        switch_entry.status = 'D'
    else:
        switch_entry.status = 'O'
    switch_entry.save()
    return redirect('overview')
