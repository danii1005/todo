from django import forms
from www.todolist import models
from django.utils.translation import ugettext_lazy as _


class TodoEntry_form(forms.ModelForm):
    class Meta:
        model = models.TodoEntry
        fields = [
            'title', 
            'description', 
            'priority', 
            'due_date', 
            'due_time'
        ]
        labels = {
            'title': _('Titel'), 
            'description': _('Beschreibung'), 
            'priority': _('Prioritaet'),
            'due_date': _('Datum'), 
            'due_time': _('Zeit'),
        }
